""" rename master branch to develop for all registered portions of this namespace package.

see also src/doc/git_move_branch_to_develop.txt.
"""
import os

from de.core import namespace_env_vars, sh_exec
from ae.console import ConsoleApp


__version__ = "0.2.0"


if __name__ == '__main__':
    root_nev = namespace_env_vars()
    namespace_name = root_nev['namespace_name']

    ca = ConsoleApp()
    ca.add_opt('gitUser', f"User name to push to registered {namespace_name} portion repositories", "")
    ca.add_opt('gitToken', "Personal access token or password to push to registered portion repositories", "")

    portions_package_names = root_nev['portions_package_names']
    repo_root = root_nev['repo_root']
    cwd = os.getcwd()

    username = ca.get_opt('gitUser') or input("Please enter username of your git remote/host:")
    password = ca.get_opt('gitToken') or input("...... enter password:")
    if not username or not password:
        ca.po(f"***   empty user name '{username}', password '{password}'")
        ca.shutdown(9)

    local_portions = list()
    for package_name in portions_package_names:
        local_portion_path = os.path.join('..', package_name)
        if not os.path.exists(local_portion_path):
            ca.po(f"**    skipping un-installed package {package_name}")
            continue

        local_portions.append(local_portion_path)
        os.chdir(local_portion_path)

        por_nev = namespace_env_vars(namespace_name=namespace_name)
        ca.po(f"..    determined portion vars:\n      {por_nev}")

        exec_ret = sh_exec("git branch -m master develop")
        if exec_ret:
            ca.po(exec_ret)
            ca.shutdown(18)

        svc_pre = "https://"
        exec_ret = sh_exec(
            f"git push -u {svc_pre}{username}:{password}@{repo_root[len(svc_pre):]}/{package_name}.git develop")
        if exec_ret:
            ca.po(exec_ret)
            ca.shutdown(27)
        ca.po(f"...   portion {package_name} moved to develop branch")

        exec_ret = sh_exec("git remote set-head origin --auto")
        if exec_ret:
            ca.po(exec_ret)
            ca.shutdown(36)

    os.chdir(cwd)                                       # reset cwd
    ca.po(f"####  moved from master to develop branch the following {len(local_portions)} portions:\n{local_portions}")

    ca.shutdown()

""" register a new portion, adding it to the ae namespace """
import distutils.dir_util
import os

from de.core import TESTS_FOLDER, code_file_version, file_content, namespace_env_vars, nev_str, patch_templates, sh_exec

from ae.paths import copy_file
from ae.console import ConsoleApp

from deploy_portions import COMMON_BASE_FILES, patch_common_base_files


TEST_FILE_NAME_PREFIX = 'test_'


if __name__ == '__main__':
    ca = ConsoleApp()
    ca.add_opt('name', "name of the portion to register", '')
    ca.add_opt('from_path', "ae folder path from where to copy the portion module/tests", '../AcuSihotInterfaces')
    ca.add_opt('onto_path', "source code root folder under where the new portion will be created", '..')

    root_nev = namespace_env_vars()
    namespace_name = nev_str(root_nev, 'namespace_name')
    cwd = os.getcwd()
    if os.path.basename(cwd) != namespace_name or not os.path.exists(nev_str(root_nev, 'PORTIONS_COMMON_DIR')):
        ca.po(f"***   Run this script at the root folder of {namespace_name} namespace package root, not from {cwd}")
        ca.shutdown(3)
    root_version = code_file_version('setup.py')
    if not root_version:
        ca.po(f"***   {cwd}/setup.py is missing or does not contain a valid version string")
        ca.shutdown(4)

    portion_name = ca.get_opt('name')
    if not portion_name:
        portion_name = input("Please enter the name of the portion to register: ")
        if not portion_name:
            ca.po("portion name has to be specified")
            ca.show_help()
            ca.shutdown(3)

    from_path = ca.get_opt('from_path')
    py_ext = nev_str(root_nev, 'PY_EXT')
    portion_from_path = os.path.join(from_path, namespace_name + "_" + portion_name, portion_name + py_ext)
    if not os.path.exists(portion_from_path):
        portion_from_path2 = os.path.join(from_path, namespace_name, portion_name + py_ext)
        if not os.path.exists(portion_from_path2):
            from_path = os.path.join("..", f"ae_{portion_name}")
            portion_from_path3 = os.path.join(from_path, namespace_name, portion_name + py_ext)
            if not os.path.exists(portion_from_path3):
                portion_from_path4 = os.path.join(from_path, namespace_name, portion_name, "__init__" + py_ext)
                if not os.path.exists(portion_from_path4):
                    ca.po(f"Portion {portion_name} not found at {portion_from_path}, {portion_from_path2},"
                          f"{portion_from_path3} nor as package at {portion_from_path4}")
                    ca.shutdown(12)
                portion_from_path = portion_from_path4
            else:
                portion_from_path = portion_from_path3
        else:
            portion_from_path = portion_from_path2

    tests_from_path = os.path.join(
        from_path, namespace_name + "_" + portion_name, TESTS_FOLDER,
        TEST_FILE_NAME_PREFIX + portion_name + py_ext)
    if not os.path.exists(tests_from_path):
        tests_from_path2 = os.path.join(
            from_path, namespace_name, TESTS_FOLDER, TEST_FILE_NAME_PREFIX + portion_name + py_ext)
        if not os.path.exists(tests_from_path2):
            tests_from_path3 = os.path.join(
                from_path, TESTS_FOLDER, TEST_FILE_NAME_PREFIX + portion_name + py_ext)
            if not os.path.exists(tests_from_path3):
                ca.po(f"Portions unit tests not found at {tests_from_path}, {tests_from_path2}"
                      f" nor at {tests_from_path3}")
                ca.shutdown(15)
            tests_from_path = tests_from_path3
        else:
            tests_from_path = tests_from_path2

    onto_path = ca.get_opt('onto_path')
    package_onto_path = os.path.join(onto_path, namespace_name + '_' + portion_name)
    if not os.path.exists(package_onto_path):
        os.mkdir(package_onto_path)

    portion_onto_path = os.path.join(package_onto_path, namespace_name)
    if not os.path.exists(portion_onto_path):
        os.mkdir(portion_onto_path)
    if portion_from_path.endswith("__init__" + py_ext):
        portion_onto_path = os.path.join(portion_onto_path, portion_name, "__init__" + py_ext)
    else:
        portion_onto_path = os.path.join(portion_onto_path, portion_name + py_ext)
    if portion_onto_path != portion_from_path:
        copy_file(portion_from_path, portion_onto_path)
        ca.po(f"..    copied portion file from {portion_from_path} onto {portion_onto_path}")
    por_nev = namespace_env_vars(namespace_name=namespace_name, root_path=package_onto_path)     # read portion env vars
    if por_nev['portion_file_path'] != os.path.abspath(portion_onto_path):
        ca.po(f"Portion file path discrepancy: {por_nev['portion_file_path']} != {cwd}/{portion_onto_path}")
        ca.shutdown(99)
    por_nev['root_version'] = root_version
    ca.po(f"..    determined and extended portion vars:\n      {por_nev}")

    tests_onto_path = os.path.join(package_onto_path, TESTS_FOLDER)
    if not os.path.exists(tests_onto_path):
        os.mkdir(tests_onto_path)
    tests_onto_path = os.path.join(tests_onto_path, TEST_FILE_NAME_PREFIX + portion_name + py_ext)
    if tests_onto_path != tests_from_path:
        copy_file(tests_from_path, tests_onto_path)
        ca.po(f"..    copied unit tests from {tests_from_path} onto {tests_onto_path}")

    copy_ret = distutils.dir_util.copy_tree(nev_str(root_nev, 'PORTIONS_COMMON_DIR'), package_onto_path, verbose=True)
    ca.po(f"..    copied common files onto {copy_ret}")

    for file_name in COMMON_BASE_FILES:
        patch_common_base_files(file_name, cwd, package_onto_path)
    ca.po(f"..    copied and patched common base files {COMMON_BASE_FILES}")

    file_name = '.python-version'
    copy_file(file_name, package_onto_path)
    ca.po(f"..    copied {file_name} to {package_onto_path}")

    os.chdir(package_onto_path)                         # step into portion package root dir

    templates = patch_templates(por_nev)
    for fn in templates:
        os.remove(fn)
    ca.po(f"..    patched and removed portion templates {templates}")

    por_req = nev_str(por_nev, 'REQ_FILE_NAME')
    if not os.path.exists(por_req):
        with open(por_req, 'w') as fh:
            fh.write(f"# dependencies of the {por_nev['portion_name']} portion\n")

    if not os.path.exists('.git'):
        if sh_exec("git init", app=ca):
            ca.shutdown(300)
        if sh_exec("git add -A", app=ca):
            ca.shutdown(330)
        if sh_exec(f"git remote add origin {por_nev['repo_root']}/{por_nev['package_name']}", app=ca):
            ca.shutdown(360)

    os.chdir(cwd)                                       # reset to cwd

    root_req = nev_str(root_nev, 'REQ_FILE_NAME')
    req_content = file_content(root_req)
    if por_nev['package_name'] not in req_content:
        with open(root_req, 'a') as fh:
            fh.write(por_nev['package_name'] + '\n')
        ca.po(f"Portion {por_nev['portion_name']} registered in"
              f" {root_req}:\n{file_content(root_req)}")
    else:
        ca.po(f"Portion {por_nev['portion_name']} got already registered in {root_req}:\n{req_content}")

    ca.shutdown()

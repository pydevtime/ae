"""
setup for root package of the ae namespace
==========================================

ReadTheDocs Server Infrastructure Configuration for this namespace root package
-------------------------------------------------------------------------------

go to `https://readthedocs.org/dashboard/<project>/edit/Admin/Advanced Settings/Default Settings`__
and put the following:

    * requirements file: requirements.txt/REQ_FILE_NAME
    * install project: check
    * use system packages: check

"""
import glob
import os
import setuptools

from de.core import file_content, namespace_env_vars, nev_str


__version__ = '0.2.83'


root_nev = namespace_env_vars()
repo_root = root_nev['repo_root']
root_license = root_nev['project_license']


data_files = list()

root_files = [_ for _ in glob.glob('.*') + glob.glob('*.*') if not os.path.isdir(_)]
data_files.append(('', root_files))

portions_common_files = [(dir_path, [os.path.join(dir_path, file_name) for file_name in files])
                         for dir_path, folders, files in os.walk(nev_str(root_nev, 'PORTIONS_COMMON_DIR'))
                         if not dir_path.endswith('__')]    # exclude __pycache__
data_files.extend(portions_common_files)

docs_path = 'docs'
docs_files = [(dir_path, [os.path.join(dir_path, file_name) for file_name in files])
              for dir_path, folders, files in os.walk(docs_path)
              if not dir_path.startswith('_')]              # exclude build folders
data_files.extend(docs_files)


setup_require = ['de_core']


if __name__ == "__main__":
    namespace_name = nev_str(root_nev, 'namespace_name')
    setuptools.setup(
        name=namespace_name,                # pip install name (not the import package name)
        version=__version__,
        author="Andi Ecker",
        author_email="aecker2@gmail.com",
        description=f"maintenance root of {namespace_name} namespace package",
        license=root_license,
        long_description=file_content("README.md"),
        long_description_content_type="text/markdown",
        url=f"{repo_root}/{namespace_name}",
        data_files=data_files,
        python_requires=">=3.6",
        install_requires=setup_require,
        setup_requires=setup_require,
        classifiers=[
            "Development Status :: 2 - Pre-Alpha",
            "Natural Language :: English",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "License :: " + root_license,
            "Operating System :: OS Independent",
            "Topic :: Software Development :: Libraries :: Application Frameworks",
        ],
        keywords=[
            'productivity',
            'application',
            'environment',
            'configuration',
            'development',
        ]
    )

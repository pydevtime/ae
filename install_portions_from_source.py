""" install portions from local portions source directories.
"""
import os

from de.core import namespace_env_vars, nev_str, nev_val, sh_exec
from ae.console import ConsoleApp


if __name__ == '__main__':
    ca = ConsoleApp()

    root_nev = namespace_env_vars()
    namespace_name = nev_str(root_nev, 'namespace_name')
    portions_package_names = nev_val(root_nev, 'portions_package_names')
    cwd = os.getcwd()
    if os.path.basename(cwd) != namespace_name:
        ca.po(f"***   Run this script at the root folder of {namespace_name} namespace package root, not from {cwd}")
        ca.shutdown(3)

    for package_name in portions_package_names:
        if not package_name.startswith(namespace_name + "_"):
            ca.po(f"Invalid local portion name prefix {package_name} (from {nev_str(root_nev, 'REQ_FILE_NAME')})")
            ca.shutdown(14)

        local_portion_path = os.path.join('..', package_name)
        if not os.path.exists(local_portion_path):
            ca.po(f"local portions path {local_portion_path} not found")
            continue

        if sh_exec(f"pip install -e {local_portion_path}"):
            ca.po(f"installation from local portions path {local_portion_path} failed")
            ca.shutdown(15)

    ca.po(f"####  (re-)installed {len(portions_package_names)} registered portions: {portions_package_names}")
    ca.shutdown()

""" deploy portion(s) including namespace common files.

this script has to be executed from the local root directory of this namespace root package. it will deploy all common
files (including the files listed in the module variable :data:`COMMON_BASE_FILES`) to the portion(s) to be deployed.

to include also any uncommitted changes in your local portion repository specify the command line flag option
`--include_uncommitted_portions`.

specify the portion package name(s) to be deployed as command line argument(s). at deploy of multiple portions specify
them in the order of dependence (the depending portion after the portion it depends on). passing `ALL` instead of
portion name(s) will deploy all the portions registered in this namespace root package.

the `--delay` command line option allows to change the delay default value between the deployment of each portion of
multiple changed portions of this namespace. e.g. set it to zero if the changed portions do not depend on each other.
increase the delay value if the time between a two depending portions is too short (to be deployed, released to PYPI
and ready for to be cloned in order to test and push a depending portion in the batch).

to deploy local changes of a local feature branch to the origin repository, specify the name of the feature branch with
the command line option `--branch`.

the portion version number(s) will be automatically incremented. the part of the version number that will be incremented
can be controlled with the command line option `--version_increment_part`.

to provide individual commit messages for each deployed portion create a file with the name `.commit_message` in the
local project folder of the(se) namespace portion(s). the content of this file will be evaluated as f-string
individually with the namespace environment vars (por_nev) of each portion. if the file content doesn't contain two line
feeds then it will be combined with the standard deploy commit message.
"""
import distutils.dir_util
import os
import tempfile
import time

from de.core import (
    bump_file_version, code_file_version, file_content, namespace_env_vars, nev_str, patch_templates, sh_exec)

from ae.base import UNSET
from ae.console import ConsoleApp
from ae.progress import Progress

try:
    from setup import portions_common_files, __version__ as local_root_version
    from prepare_commit import main as prepare_commit_main
except ImportError:
    portions_common_files = local_root_version = prepare_commit_main = None


GIT_AUTH_METHOD = 'HTTPS'   # 'API' clone/push not implemented in GitHub-API, 'STDIN' freezes push (waiting for input)
COMMON_FILE_NOTE = "THIS FILE IS EXCLUSIVELY MAINTAINED IN THE NAMESPACE ROOT PACKAGE. CHANGES HAVE TO BE DONE THERE."
COMMON_BASE_FILES = ('CONTRIBUTING.rst', 'LICENSE.md', 'SECURITY.md', '.gitignore')

local_root_pkg_dir = os.getcwd()
root_nev = namespace_env_vars()
namespace_name = nev_str(root_nev, 'namespace_name')


def exit_error(error_code: int, error_message: str = ""):
    """ quit this shell script, optionally displaying an error message. """
    if error_code < 6:
        ca.show_help()
    if error_message:
        ca.po(error_message)
    ca.shutdown(error_code)


def update_commit_portion(portion_name: str, branch_name: str, patch_dir: str, portion_dir: str, root_version: str
                          ) -> str:
    """ update/patch common files of the portion project in the current working directory and commit. """
    if sh_exec(f"git checkout {branch_name}"):
        exit_error(42)
    ca.po(f" ##   checked out {branch_name} branch of portion package {portion_name} in {os.getcwd()}")

    temp_root_pkg_dir = os.path.join(patch_dir, namespace_name)
    common_files_dir = os.path.join(temp_root_pkg_dir, nev_str(root_nev, 'PORTIONS_COMMON_DIR'))  # tmp root
    copy_ret = distutils.dir_util.copy_tree(common_files_dir, portion_dir, verbose=True)
    ca.po(f"  #   copied to {portion_dir} the portions common files: {copy_ret}")

    old_nev = namespace_env_vars(namespace_name=namespace_name, root_path=portion_dir)
    version_part = ca.get_opt('version_increment_part')
    err = bump_file_version(old_nev['portion_file_path'], version_part=version_part)
    if err:
        exit_error(48, f"****  bump version error {err} for file {old_nev['portion_file_path']}")
    por_nev = namespace_env_vars(namespace_name=namespace_name, root_path=portion_dir)  # reread bumped version
    por_nev['root_version'] = root_version
    por_version = por_nev["package_version"]
    ca.po(f"  #   bumped portion version {old_nev['package_version']} to {por_version}; env vars:\n      {por_nev}")

    for file_name in COMMON_BASE_FILES:
        patch_common_base_files(file_name, temp_root_pkg_dir, portion_dir)
    ca.po(f"  #   copied and patched common base files {COMMON_BASE_FILES}")

    templates = patch_templates(por_nev)
    for file_name in templates:
        os.remove(file_name)
    ca.po(f"  #   patched and removed portion templates {templates}")

    release_portion = ca.get_opt('release')
    por_nev['deploy_action'] = 'release' if release_portion else 'deploy'
    file_name = ".commit_message"
    commit_msg = "V {package_version}: {deploy_action} common files V {root_version}"
    if not os.path.exists(file_name):
        commit_msg += "."
    else:
        msg = file_content(file_name)
        commit_msg = msg if "\n\n" in msg else commit_msg + ":\n\n" + msg
        os.remove(file_name)
    commit_msg = commit_msg.format(**por_nev).strip()

    if sh_exec("git add -A"):  # needed to add/remove common/base files
        exit_error(51)
    if sh_exec("git commit", ("-m", commit_msg)):
        exit_error(54)
    ca.po(f" ##   portion {portion_name} committed")

    if release_portion:
        if sh_exec("git tag -a", (f"v{por_version}", "-m", commit_msg)):
            exit_error(57)
        ca.po(f" ##   portion {portion_name} tagged with {por_version}")

    return por_version


def patch_common_base_files(file_name: str, src: str, dst: str):
    """ copy the src/file_name, add portion header and save into dst/file_name """
    ext = os.path.splitext(file_name)[1]
    if ext == '.md':
        beg, end = "<!--", "-->"
    elif ext == '.rst':
        beg, end = "\n..\n   ", "\n"
    else:
        beg, end = "#", ""
    content = f"{beg} {COMMON_FILE_NOTE} {end}\n{file_content(os.path.join(src, file_name))}"
    with open(os.path.join(dst, file_name), 'w') as fp:
        fp.write(content)


def push_portion(portion_name: str, branch_name: str, usr: str, pwd: str, push_tags: bool = False):
    """ push portion in the current working directory to the specified branch. """
    svc = "https://"
    repo_root = root_nev['repo_root']
    args = ["--tags"] if push_tags else []
    console_input = ""
    if GIT_AUTH_METHOD == 'STDIN':
        args.append("origin")
        console_input = f"{usr}\n{pwd}\n"
    elif GIT_AUTH_METHOD == 'HTTPS' and repo_root.startswith(svc):
        args.append(f"{svc}{usr}:{pwd}@{repo_root[len(svc):]}/{portion_name}.git")
    else:
        exit_error(60, f"invalid authentication method {GIT_AUTH_METHOD} against {repo_root}")

    if sh_exec("git push", extra_args=args + [branch_name], console_input=console_input):
        exit_error(61)
    ca.po(f" ##   portion {portion_name} pushed to remote (origin/{branch_name})")


def main():
    """ check local repos and environment, update repos, push them to origin remote, pull updates to local repos. """
    branch_name = ca.get_opt('branch')
    release_portion = ca.get_opt('release')
    if release_portion and branch_name != 'develop':
        exit_error(5, f"--release flag option can only be used in the branch develop (not in {branch_name})")
    deploy_action = 'release' if release_portion else 'deploy'

    portions_package_names = ca.get_arg('portion')
    if portions_package_names == ['ALL']:
        portions_package_names = root_nev['portions_package_names']
    ca.dpo(f"#     {deploy_action} {len(portions_package_names)} {namespace_name} portions: {portions_package_names}")
    # check state of the local repos of the namespace root package and all namespace portions

    for target_folder, file_names in portions_common_files:     # ensure common files marked as maintained externally
        for fn in file_names:
            if COMMON_FILE_NOTE not in file_content(fn):
                exit_error(6, f"****  portions common file {fn} is missing the note {COMMON_FILE_NOTE}")

    # ensure common files are unchanged in the local root repo - ALT: git diff --name-only HEAD files...
    uncommitted_commons = list()
    if sh_exec(f"git ls-files -m", COMMON_BASE_FILES + (nev_str(root_nev, 'PORTIONS_COMMON_DIR'), ),
               lines_output=uncommitted_commons):
        exit_error(9)
    # .. as well as any changed code in local machine portions repos
    uncommitted_portions = list()
    not_on_branch = list()
    for portion_name in portions_package_names:
        if not portion_name.startswith(namespace_name + "_"):
            exit_error(12, f"****  invalid portion name prefix {portion_name} ({nev_str(root_nev, 'REQ_FILE_NAME')})")

        local_portion_path = os.path.join('..', portion_name)
        if not os.path.exists(local_portion_path):
            ca.po(f" **   skipping un-installed package {portion_name}")
            continue

        os.chdir(local_portion_path)

        branch_list = list()
        if sh_exec("git branch --no-color", lines_output=branch_list):
            exit_error(13)
        local_branch = next((line[2:] for line in branch_list if line.startswith("* ")), "")
        if local_branch != branch_name:
            not_on_branch.append((portion_name, local_branch))

        captured = list()
        if sh_exec("git ls-files -m", lines_output=captured):
            exit_error(15)
        if captured:
            uncommitted_portions.append((portion_name, captured))

    os.chdir(local_root_pkg_dir)                                       # reset cwd

    ca.dpo(f"   #  local portion repos not on branch {branch_name}: {not_on_branch}")
    if uncommitted_commons:
        ca.po(f"****  common files are having uncommitted changes (commit first!): {uncommitted_commons}")

    uncommitted_other_branch = [pn for pn, _f in not_on_branch if pn in (pn for pn, _b in uncommitted_portions)]
    include_uncommitted_portions = ca.get_opt('include_uncommitted_portions')
    if uncommitted_portions:
        if not include_uncommitted_portions:
            ca.po(f"****  {len(uncommitted_portions)} local portions with uncommitted changes. commit first or specify")
            ca.po(f"      the --include_uncommitted_portions command line option. list of uncommitted portions/files:")
        elif uncommitted_other_branch:
            ca.po(f"****  {len(uncommitted_other_branch)} uncommitted portions are not on the {branch_name} branch:")
            for portion in uncommitted_other_branch:
                ca.po("      ...", portion)
            ca.po(f"      {len(uncommitted_portions)} uncommitted portions/files:")
        else:
            ca.po(f"   #  warning: {len(uncommitted_portions)} local portions are uncommitted in {branch_name} branch:")
        for portion in uncommitted_portions:
            ca.po("      ...", portion)
    if uncommitted_commons or (uncommitted_portions and (not include_uncommitted_portions or uncommitted_other_branch)):
        exit_error(18)

    usr = ca.get_opt('gitUser') or input("Please enter username of your git remote/host:")
    pwd = ca.get_opt('gitToken') or input("...... enter password:")
    if not usr or not pwd:
        exit_error(19, f"****  empty user name '{usr}' or password '{pwd}'")

    # prepare deploy of common files

    prepare_commit_main()           # ensure that all templates get converted

    with tempfile.TemporaryDirectory() as patch_dir:
        os.chdir(patch_dir)
        ca.po(f" ##   created and changed into temporary directory {os.getcwd()}")

        repo_root = root_nev['repo_root']
        if sh_exec(f"git clone {repo_root}/{namespace_name}.git"):
            exit_error(21)
        os.chdir(namespace_name)
        root_branch = branch_name
        if sh_exec(f"git checkout {branch_name}"):
            if sh_exec("git checkout develop"):
                exit_error(22)
            root_branch = 'develop'
        os.chdir(patch_dir)
        ca.po(f" ##   cloned {namespace_name} namespace root and checked out {root_branch} branch")

        root_version = code_file_version(os.path.join(namespace_name, 'setup.py'))      # of cloned root repo
        if not root_version or root_version != local_root_version:
            exit_error(24, f" ***  {os.path.join(namespace_name, 'setup.py')} or contained version string missing")

        portions_progress = Progress(ca, start_counter=len(portions_package_names))
        local_portions_to_pull = list()
        for portion_name in portions_package_names:
            portions_progress.next(processed_id=portion_name)

            uncommitted_portion = portion_name in (pn for pn, _f in uncommitted_portions)
            if include_uncommitted_portions and uncommitted_portion:
                portion_dir = os.path.join(local_root_pkg_dir, '..', portion_name)
                ca.po(f" ##   using uncommitted portion package {portion_name} in local src dir {portion_dir}")
            else:
                portion_dir = os.path.join(patch_dir, portion_name)
                if sh_exec(f"git clone {repo_root}/{portion_name}.git"):
                    exit_error(36)
                ca.po(f" ##   cloned portion package {portion_name} into temporary patch directory {patch_dir}")
                local_portions_to_pull.append(portion_name)
            os.chdir(portion_dir)

            portion_version = update_commit_portion(portion_name, branch_name, patch_dir, portion_dir, root_version)
            push_portion(portion_name, branch_name, usr, pwd, push_tags=release_portion)
            if release_portion:
                release_branch = 'release' + portion_version
                if sh_exec(f"git checkout -b {release_branch} develop"):
                    exit_error(84)
                ca.po(f"  #   created and checked out {release_branch} branch in {os.getcwd()}")
                push_portion(portion_name, release_branch, usr, pwd)
                if uncommitted_portion:
                    if sh_exec(f"git checkout develop"):
                        exit_error(87)
                    ca.po(f"  #   created and checked out {release_branch} branch in {os.getcwd()}")

            delay_seconds = ca.get_opt('delay')
            if delay_seconds > 0 and portion_name != portions_package_names[-1]:
                delay_progress = Progress(ca, start_counter=delay_seconds, start_msg="...pausing {run_counter} seconds")
                while delay_seconds:
                    time.sleep(0.999999)
                    delay_progress.next(processed_id=portion_name + " post-deploy-delay")
                    delay_seconds -= 1

            os.chdir(patch_dir)                         # go back to temp patching dir

    portions_progress.finished()
    ca.po(f"####  removed temporary folder {patch_dir}")

    os.chdir(local_root_pkg_dir)                                       # reset cwd

    # pull updated portions repos onto local machine

    pull_err_portions = list()
    for portion_name in local_portions_to_pull:
        os.chdir(os.path.join("..", portion_name))
        if sh_exec(f"git pull -v origin {branch_name}"):
            pull_err_portions.append(portion_name)
    if pull_err_portions:
        exit_error(90, f"****  local git pull errors occurred in portion(s): {pull_err_portions}")

    os.chdir(local_root_pkg_dir)                                       # reset cwd

    ca.po(f"####  successful {deploy_action} of portions common files: {portions_common_files}")
    ca.po(f"      including the patched common base files: {COMMON_BASE_FILES}")
    ca.po(f"      onto the {len(portions_package_names)} portions: {portions_package_names}")


if __name__ == '__main__':
    ca = ConsoleApp(app_version=local_root_version)
    ca.add_opt('branch', "branch name to deploy to (default=develop)", 'develop')
    ca.add_opt('delay', "delay in seconds to wait after deploy each portion", 300)
    ca.add_opt('gitUser', f"git user name to push to registered {namespace_name} portion repositories", "")
    ca.add_opt('gitToken', "git personal access token or password to push to registered portion repositories", "")
    ca.add_opt('include_uncommitted_portions', "include locally uncommitted portion changes of same branch", UNSET)
    ca.add_opt('release', "deploy portions to PYPI (tagged and as release branch)", UNSET)
    ca.add_opt('version_increment_part', "portion version number part to increment (1=mayor, 2=namespace, 3=patch)", 3,
               choices=range(1, 4))
    ca.add_arg('portion', help='portion name(s) or ALL (for all namespace portions)', nargs='+')

    if not portions_common_files or not local_root_version or not prepare_commit_main:
        exit_error(3, f"****  invalid environment {os.getcwd()}, run this script from namespace root package folder")
    if not os.path.exists(nev_str(root_nev, 'PORTIONS_COMMON_DIR')):
        exit_error(4, f"****  invalid working dir {os.getcwd()}, run this script from namespace root package folder")

    ca.run_app()
    main()
    ca.shutdown()

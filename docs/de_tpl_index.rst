application environment for python
##################################

welcome to the {namespace_name} namespace documentation.

the portions (modules and sub-packages) of this freely extendable namespace (:pep:`420`) are providing helper functions
and classes for your python application, resulting in less code for you to write and maintain, in the areas/domains::

    * data processing/validation
    * file handling
    * i18n (localization)
    * configuration settings
    * console
    * logging
    * database access
    * networking
    * multi-platform/-OS
    * context help
    * app tours
    * user preferences (font size, color, theming, ...)
    * QR codes
    * sideloading


screenshots of apps developed with ae portions
**********************************************


.. list-table::

    * - .. figure:: _images/glsl_tester_demo.gif
           :scale: 90 %
           :target: _images/glsl_tester_demo.gif

           GlslTest app demo


.. list-table::

    * - .. figure:: _images/kivy_lisz_root.png
           :alt: root list of a dark themed kivy lisz app
           :scale: 30 %
           :target: _images/kivy_lisz_root.png

           kivy lisz app root list

      - .. figure:: _images/kivy_lisz_fruits.png
           :alt: fruits sub-list of a dark themed kivy lisz app
           :scale: 30 %
           :target: _images/kivy_lisz_fruits.png

           fruits sub-list

      - .. figure:: _images/kivy_lisz_fruits_light.png
           :alt: fruits sub-list of a light themed kivy lisz app
           :scale: 30 %
           :target: _images/kivy_lisz_fruits_light.png

           using light theme

    * - .. figure:: _images/kivy_lisz_user_prefs.png
           :alt: user preferences drop down
           :scale: 30 %
           :target: _images/kivy_lisz_user_prefs.png

           lisz user preferences

      - .. figure:: _images/kivy_lisz_color_editor.png
           :alt: kivy lisz color editor
           :scale: 30 %
           :target: _images/kivy_lisz_color_editor.png

           kivy lisz color editor

      - .. figure:: _images/kivy_lisz_font_size_big.png
           :alt: lisz app using bigger font size
           :scale: 30 %
           :target: _images/kivy_lisz_font_size_big.png

           bigger font size


.. list-table::
    :widths: 27 66

    * - .. figure:: _images/enaml_lisz_fruits_sub_list.png
           :alt: fruits sub-list of dark themed enaml lisz app
           :scale: 27 %
           :target: _images/enaml_lisz_fruits_sub_list.png

           enaml/qt lisz app

      - .. figure:: _images/enaml_lisz_light_landscape.png
           :alt: fruits sub-list of a light themed enaml lisz app in landscape
           :scale: 66 %
           :target: _images/enaml_lisz_light_landscape.png

           light themed in landscape




code maintenance guidelines
***************************


portions code requirements
==========================

    * pure python
    * fully typed (:pep:`526`)
    * fully :ref:`documented <{namespace_name}-portions>`
    * 100 % test coverage
    * multi thread save
    * code checks (using pylint and flake8)


design pattern and software principles
======================================

    * `DRY <http://en.wikipedia.org/wiki/Don%27t_repeat_yourself>`_
    * `KISS <http://en.wikipedia.org/wiki/Keep_it_simple_stupid>`_


.. include:: ../CONTRIBUTING.rst


add new namespace portion
=========================

follow the steps underneath to add a new portion to the `{namespace_name}` namespace. the steps
7 to 9 can be realized by the script `register_portion.py`, which can also be used or adopted
to migrate an existing packages from other projects into this namespace:

1. choose a not-existing/unique name for the new portion (referred as `<portion-name>` in the next steps).
2. create a new project folder `{namespace_name}_<portion-name>` in the source code directory of your local machine.
3. in the new project folder create/prepare a local virtual environment with `pyenv <https://pypi.org/project/pyenv/>`_.
4. create new test module in the `tests` folder within the new code project folder and then code your unit tests.
5. create the `{namespace_name}` namespace sub-folder within the new code project folder.
6. within the `{namespace_name}` folder create new module or sub-package, implement the new portion
   and set the __version__ variable to '0.0.1'.
7. copy all files from the folder `{PORTIONS_COMMON_DIR}` of this package into the new code project folder.
8. init a new git repository on your local machine and add all files.
9. create new project/repository with the name `{namespace_name}_<portion-name>` (in the GitLab
   group `{namespace_name}-group` {repo_root}).
10. hack/test/fix/commit until all tests succeed.
11. complete type annotations and the docstrings to properly document the new portion.
12. finally push the new namespace portion repository onto `{repo_root}/{namespace_name}_<portion-name>`.

.. note::
    if you want to put the portion separately into your own repository (see step 11 above) then you also have
    to add two protected vars PYPI_USERNAME and PYPI_PASSWORD (mark also as masked)
    and provide the user name and password of your PYPI account (on Gitlab.com at Settings/CI_CD/Variables).

.. hint::
    with the push to GitLab (done in step 12 above) the CI jobs will automatically run all tests and
    then publish the new portion onto PYPI.

to request the registration of the new portion to one of the {namespace_name} namespace maintainers - this
way the new portion will be included into the `{namespace_name} namespace package` documentation on
`ReadTheDocs <https://{namespace_name}.readthedocs.io>`_.


change common files of all portions
===================================

any change on one of the common files (situated in the `{PORTIONS_COMMON_DIR}` folder of this repository) and to the
common base files of this namespace have to be done from/via this namespace root package.

use the script `deploy_portions.py` to distribute changes of your local portions or of the common portion files.

.. note::
   all changes to the common files of this root package have first to be pushed to the public git repository
   before they can be distributed to the namespace portion packages.


.. _{namespace_name}-portions:

registered namespace package portions
*************************************

the following list contains all registered portions of the
{namespace_name} namespace. most portions are single module
files, some of them are sub-packages.


.. hint::
    portions with no dependencies are at the begin of the following
    list. the portions that are depending on other portions of the
    {namespace_name} namespace are listed more to the end.


.. autosummary::
    :toctree: _autosummary
    :nosignatures:

    {portions_import_names}


indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* `portion repositories at GitLab <{repo_root}>`_

""" prepare commit to ae root repository. """
import os
import sys

from de.core import namespace_env_vars, nev_str, patch_templates


def main():
    """ patch all templates at and under the current working directory. """
    root_nev = namespace_env_vars()
    por_dir = nev_str(root_nev, 'PORTIONS_COMMON_DIR')
    cwd = os.getcwd()
    if os.path.basename(cwd) != nev_str(root_nev, 'namespace_name') or not os.path.exists(por_dir):
        print(f"Start this script from the root folder of the project, not from {cwd}")
        sys.exit(126)   # most OS only support integers in the range 1...127, ubuntu displays e.g. 77 for error code 333

    print(f"##    Patching all templates underneath of {cwd} with {root_nev}, excluding only portions common files")
    patched = patch_templates(root_nev, por_dir + os.path.sep, 'docs' + os.path.sep + '_')
    print(f"##    Successfully patched {len(patched)} files from templates {patched}")


if __name__ == '__main__':
    main()

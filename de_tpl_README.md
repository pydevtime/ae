# maintaining {namespace_name} namespace package

the __{namespace_name}__ (application environment) namespace is providing
[helper modules and packages](https://{namespace_name}.readthedocs.io) to ease the implementation and maintenance of
applications written in Python.

this repository is maintaining all the portions (modules and sub-packages) of the {namespace_name} namespace to:

* merge docstrings of all portions into one documentation
* publish documentation via Sphinx onto [ReadTheDocs](https://{namespace_name}.readthedocs.io "{namespace_name} on RTD")
* provide common files to all portions
* deploy changes on common files to all portions 

never add this root package of the {namespace_name} namespace into your application dependencies, instead only add the
namespace portions that are directly used by your app.

the following portions are currently included in this namespace:

{portions_pypi_refs_md}

de_core
kivy==2.0.0
plyer
# RTD-build is failing if the Sphinx dependency is included without a pinned version (RTD was using 1.8.5 by default).
# Also Sphinx 3.0 is showing lots of warnings: https://github.com/sphinx-doc/sphinx/issues/7429 recommends removing the
# typehints extension (currently not changed/touched). WORKAROUND: pin Sphinx version here (used then by RTD too).
# since V 0.2.78 (11.06.2021) switched Sphinx from 3.5.4 to 4.0.2
# since V 0.2.79 (19.07.2021) pinned Sphinx to 4.1.1
sphinx==4.1.1
sphinx_autodoc_typehints
# RTD-build default for sphinx_rtd_theme is <0.5 (0.4.3 get used), pin to current version 0.5.1/.2
# STRANGE: DOES NOT FIND IT USING UNDERSCORE CHARACTERS IN PACKAGE NAME: sphinx_rtd_theme
sphinx-rtd-theme==0.5.2
sphinx_paramlinks
# (currently 39) ae portions got registered by this namespace root/doc/maintenance project
ae_base
ae_deep
ae_droid
ae_valid
ae_files
ae_paths
ae_core
ae_lockname
ae_inspector
ae_i18n
ae_parse_date
ae_literal
ae_progress
ae_updater
ae_console
ae_sys_core
ae_sys_data
ae_sys_core_sh
ae_sys_data_sh
ae_db_core
ae_db_ora
ae_db_pg
ae_transfer_service
ae_sideloading_server
ae_gui_app
ae_gui_help
ae_lisz_app_data
ae_kivy_glsl
ae_kivy_auto_width
ae_kivy_dyn_chi
ae_kivy_relief_canvas
ae_kivy_help
ae_kivy_app
ae_kivy_user_prefs
ae_kivy_file_chooser
ae_kivy_qr_displayer
ae_kivy_iterable_displayer
ae_kivy_sideloading
ae_enaml_app
